angular.module('eventServices', [])

    .factory('Event', function ($http) {
        var eventFactory = {}; // Create the eventFactory object

        // Create events in database
        eventFactory.create = function (evData) {
            return $http.post('/api/event/create', evData);
        };

        // Get all the users from database
        eventFactory.getEvents = function () {
            return $http.get('/api/event/list/');
        };


        // Get user to then edit
        eventFactory.getEvent = function (id) {
            return $http.get('/api/event/edit/' + id);
        };

        eventFactory.getMyEvent = function () {
            return $http.get('/api/event/my/');
        };

        return eventFactory; // Return userFactory object
    });