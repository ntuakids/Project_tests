// Creates the gservice factory. This will be the primary means by which we interact with Google Maps
angular.module('googleServices', [])
    .factory('googleServices', function($http){

      var googleFactory = {}; // Create the googleFactory object
      var location;
      var markers = [];
      var infowindows = [];

        googleFactory.refresh = function (filteredResults) {
          this.clearMarkers();
          console.log(filteredResults)
          if (filteredResults){

            for(var i= 0; i < filteredResults.length; i++) {

              location = {
                  lat: filteredResults[i].location[0],
                  lng: filteredResults[i].location[1]
              };

              this.addMarkerWithTimeout(filteredResults[i].title, filteredResults[i].address, 
                                              location, filteredResults[i]._id, i*200)
            }
          }
        
        };

      googleFactory.addMarkerWithTimeout = function (title, address, position, id, timeout) {

        var marker = new google.maps.Marker({
            position: position,
            map: map,
            animation: google.maps.Animation.DROP
          });

        window.setTimeout(function() {
          markers.push(marker);
        }, timeout);

        var infowindow = new google.maps.InfoWindow({
          content: '<div><strong>' + title + '</strong><br>' + address + '<br> <a href="/show/event/' + id + '"> Περισσότερα </a>' + '</div>',
          maxWidth: 200
        });

        marker.addListener('click', function() {
          infowindow.open(map, marker);
        });
      }

      googleFactory.clearMarkers = function (){
        for (var i = 0; i < markers.length; i++) {
          markers[i].setMap(null);
        }
        markers = [];       
      }

      return googleFactory;
});
