angular.module('eventManagController', [])

    // Controller: User to control the management page and managing of user accounts
    .controller('eventManagCtrl', function (Event, $scope) {
        var app = this;

        app.loading = true; // Start loading icon on page load
        app.accessDenied = true; // Hide table while loading
        app.errorMsg = false; // Clear any error messages
        app.editAccess = false; // Clear access on load
        app.deleteAccess = false; // CLear access on load
        app.limit = 5; // Set a default limit to ng-repeat
        app.searchLimit = 0; // Set the default search page results limit to zero

        // Function: get all the events from database
        function getEvents() {
            // Runs function to get all the events from database
            Event.getEvents().then(function (data) {
                // Check if able to get data from database
                if (data.data.success) {
                    // Check which permissions the logged in user has
                    if (data.data.permission === 'admin' || data.data.permission === 'provider') {
                        app.events = data.data.events; // Assign events from database to variable
                        app.loading = false; // Stop loading icon
                        app.accessDenied = false; // Show table
                        // Check if logged in user is an admin or provider
                        if (data.data.permission === 'admin') {
                            app.editAccess = true; // Show edit button
                            app.deleteAccess = true; // Show delete button
                        } else if (data.data.permission === 'provider') {
                            app.editAccess = true; // Show edit button
                        }
                    } else {
                        app.errorMsg = 'Insufficient Permissions'; // Reject edit and delete options
                        app.loading = false; // Stop loading icon
                    }
                } else {
                    app.errorMsg = data.data.message; // Set error message
                    app.loading = false; // Stop loading icon
                }
            });
        }

        getEvents(); // Invoke function to get events from databases

        /*
        // Function: Show more results on page
        app.showMore = function(number) {
            app.showMoreError = false; // Clear error message
            // Run functio only if a valid number above zero
            if (number > 0) {
                app.limit = number; // Change ng-repeat filter to number requested by user
            } else {
                app.showMoreError = 'Please enter a valid number'; // Return error if number not valid
            }
        };
    
        // Function: Show all results on page
        app.showAll = function() {
            app.limit = undefined; // Clear ng-repeat limit
            app.showMoreError = false; // Clear error message
        };
    
        // Function: Delete a user
        app.deleteUser = function(username) {
            // Run function to delete a user
            User.deleteUser(username).then(function(data) {
                // Check if able to delete user
                if (data.data.success) {
                    getevents(); // Reset events on page
                } else {
                    app.showMoreError = data.data.message; // Set error message
                }
            });
        };
    
        // Function: Perform a basic search function
        app.search = function(searchKeyword, number) {
            // Check if a search keyword was provided
            if (searchKeyword) {
                // Check if the search keyword actually exists
                if (searchKeyword.length > 0) {
                    app.limit = 0; // Reset the limit number while processing
                    $scope.searchFilter = searchKeyword; // Set the search filter to the word provided by the user
                    $scope.searchFilter2 = 'Αντρέας';
                    app.limit = number; // Set the number displayed to the number entered by the user
                } else {
                    $scope.searchFilter = undefined; // Remove any keywords from filter
                    app.limit = 0; // Reset search limit
                }
            } else {
                $scope.searchFilter = undefined; // Reset search limit
                app.limit = 0; // Set search limit to zero
            }
        };
    
        // Function: Clear all fields
        app.clear = function() {
            $scope.number = 'Clear'; // Set the filter box to 'Clear'
            app.limit = 0; // Clear all results
            $scope.searchKeyword = undefined; // Clear the search word
            $scope.searchFilter = undefined; // Clear the search filter
            app.showMoreError = false; // Clear any errors
        };
    
        // Function: Perform an advanced, criteria-based search
        app.advancedSearch = function(searchByUsername, searchByEmail, searchByName) {
            // Ensure only to perform advanced search if one of the fields was submitted
            if (searchByUsername || searchByEmail || searchByName) {
                $scope.advancedSearchFilter = {}; // Create the filter object
                if (searchByUsername) {
                    $scope.advancedSearchFilter.username = searchByUsername; // If username keyword was provided, search by username
                }
                if (searchByEmail) {
                    $scope.advancedSearchFilter.email = searchByEmail; // If email keyword was provided, search by email
                }
                if (searchByName) {
                    $scope.advancedSearchFilter.name = searchByName; // If name keyword was provided, search by name
                }
                app.searchLimit = undefined; // Clear limit on search results
            }
        };
    
        // Function: Set sort order of results
        app.sortOrder = function(order) {
            app.sort = order; // Assign sort order variable requested by user
        };*/
    })

    // Controller: Used to edit events
    .controller('eventShowCtrl', function ($scope, $routeParams, Event, $timeout) {
        var app = this;

        // Function: get the user that needs to be edited
        Event.getEvent($routeParams.id).then(function (data) {
            // Check if the user's _id was found in database
            if (data.data.success) {
                console.log(data.data.event.title);
                app.event = data.data.event;
                app.event.free = data.data.event.avail + "/" + data.data.event.total;
                console.log(app.event);
            } else {
                app.errorMsg = data.data.message; // Set error message
                $scope.alert = 'alert alert-danger'; // Set class for message
            }
        });
    })

    .controller('MyeventShowCtrl', function ($scope, $routeParams, Event, $timeout) {
        var app = this;

        // Function: get the user that needs to be edited
        Event.getMyEvent().then(function (data) {
            // Check if the user's _id was found in database
            if (data.data.success) {
                app.events = data.data.events;
            } else {
                app.errorMsg = data.data.message; // Set error message
                $scope.alert = 'alert alert-danger'; // Set class for message
            }
        });
    })
