angular.module('payControllers', ['userServices'])

    .controller('payCtrl', function ($route, $http, $location, $timeout, User, $scope) {

        var app = this;
        app.loading = false; // Activate bootstrap loading icon
        app.errorMsg = false; // Clear errorMsg each time user submits

        this.paynow = function (data) {

            console.log(data);
            app.loading = true;
            
            User.paySub(data).then(function (data) {
                console.log(data);
                app.loading = false; // Stop bootstrap loading icon
                $scope.alert = 'alert alert-success'; // Set ng class
                app.successMsg = 'Επιτυχής πληρωμή' + '...!'; // Create Success Message then redirect
                // Redirect to home page after two milliseconds (2 seconds)
                $timeout(function() {
                    $location.path('/'); // Redirect to home
                }, 4000);
            });
        };
    });