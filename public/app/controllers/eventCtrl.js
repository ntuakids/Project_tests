angular.module('eventControllers', ['authServices', 'eventServices'])

	.controller('createCtrl', function ($scope, $http, $location, $timeout, Event, Auth) {

		var app = this;
		
		this.slider = {
			min: 3,
			max: 15,
			options: {
				floor: 3,
			    ceil: 15
			}
		};

		this.refreshSlider = function () {
	        $timeout(function () {
	            $scope.$broadcast('rzSliderForceRender');
	        });
	    };

		this.crEvent = function (evData) {
			app.loading = true;
			app.errorMsg = false;

			if (evData == null || evData.title === null || evData.description === null || 
				evData.address === null || evData.price === null || evData.total === null || 
				app.evData.latitude == null || app.evData.longitude == null ) {
				app.loading = false;
				app.errorMsg = 'Παρακαλώ συμπληρώστε όλα τα πεδία';
			} else {
				if (evData.date) {
					var tempdate = evData.date.split(" ");
					app.evData.datefull = evData.date;
					app.evData.date = tempdate[0];
					app.evData.time = tempdate[1];
					console.log(app.evData.date);
					console.log(app.evData.time);
					console.log(app.evData.datefull);
				}
				app.evData.min_age = app.slider.min;
				app.evData.max_age = app.slider.max;

				if ($scope.myFile) {
					var file = $scope.myFile;
					console.log(file);
					file.newname = Date.now() + file.name;
					app.photo = file.newname;
					console.log(file);
					var uploadUrl = "/api/upload";
					var fd = new FormData();
					fd.append('file', file);

					$http.post(uploadUrl, fd, {
						transformRequest: angular.identity,
						headers: { 'Content-Type': undefined }
					}).then(function (data) {
						//console.log(data.data.photo.filename);
						app.evData.picture = '/upload/event/images/' + data.data.photo.filename;
						//console.log(app.evData.picture);
						Auth.getUser().then(function (data) {
							app.evData.provider = data.data.id;
							Event.create(app.evData).then(function (data) {
								if (data.data.success) {
									app.loading = false;
									app.successMsg = data.data.message + '... ';
									$timeout(function () {
										$location.path('/');
									}, 2000);
								} else {
									app.loading = false;
									app.errorMsg = data.data.message;
								}
							});
						});
					});
				} else {
					Auth.getUser().then(function (data) {
						app.evData.provider = data.data.id;
						Event.create(app.evData).then(function (data) {
							if (data.data.success) {
								app.loading = false;
								app.successMsg = data.data.message + '... ';
								$timeout(function () {
									$location.path('/');
								}, 2000);
							} else {
								app.loading = false;
								app.errorMsg = data.data.message;
							}
						});

					});
				}
			}
		};
	});