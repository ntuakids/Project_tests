angular.module('queryController', ['geolocation', 'googleServices'])

	.controller('queryCtrl', function ($scope, $log, $http, $rootScope, geolocation, googleServices, Event) {

		var queryBody = {};
		var filteredEvents=[];

		 this.queryForm = function (queryData){
			//console.log(this.queryData);

			queryBody = {
				distance: parseFloat(this.queryData.distance),
				category: this.queryData.category,
				sorting: this.queryData.sorting
			};
			//console.log(queryBody);

     /* Auth.getUser().then(function (data) {
        console.log(data)
    })*/

			// Post the queryBody to the /query POST route to retrieve the filtered results
        	$http.post('/api/event/query', queryBody)

            // Store the filtered results in queryResults
            .then(function onSuccess(queryResults){
            	// Query Body and Result Logging
                console.log("QueryBody:");
                console.log(queryBody);
                console.log("QueryResults:");
                console.log(queryResults);

                filteredEvents = queryResults.data;

                googleServices.refresh(filteredEvents);

			})
            .catch(function onError(queryResults){
                console.log('Error ' + queryResults);
            })
		};

		this.initialize = function() {      // display map with all markers
			//var body = document.getElementsByTagName("body")[0];
			var putmap = document.getElementById('putmap');
			var newDiv = document.createElement("div");
  		newDiv.setAttribute("id", "map");
  		newDiv.style.width = "100%";
  		newDiv.style.height = "450px";
			putmap.appendChild(newDiv);
     	map = new google.maps.Map(document.getElementById('map'), {
             center: {lat: 37.959509, lng: 23.728943},
             zoom: 11
      });

      Event.getEvents().then(function (res) {
      var allEvents = res.data.events;
      console.log(res.data)

      googleServices.refresh(allEvents);

      })
    } 

});
