angular.module('userApp', ['payControllers', 'appRoutes', 'rzModule', 'angularjs-datetime-picker', 'userControllers', 'userServices', 'ngAnimate','eventManagController', 'mainController', 'authServices', 'emailController', 'managementController', 'eventControllers', 'eventServices', 'queryController'])

.config(function($httpProvider) {
    $httpProvider.interceptors.push('AuthInterceptors');
})
.directive('googleplace', function() {
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, model) {
            var options = {
                types: [],
                componentRestrictions: {
                    country: 'GR'
                }
            };
            scope.gPlace = new google.maps.places.Autocomplete(element[0], options);

            google.maps.event.addListener(scope.gPlace, 'place_changed', function() {
                var place = scope.gPlace.getPlace();
                //console.log(place.geometry.location.lat());
                scope.register.regData.latitude = place.geometry.location.lat();
                scope.register.regData.longitude = place.geometry.location.lng();
                scope.$apply(function() {
                    model.$setViewValue(element.val());                
                });
            });
        }
    };
})
.directive('googleplace1', function() {
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, model) {
            var options = {
                types: [],
                componentRestrictions: {
                    country: 'GR'
                }
            };
            scope.gPlace = new google.maps.places.Autocomplete(element[0], options);

            google.maps.event.addListener(scope.gPlace, 'place_changed', function() {
                var place = scope.gPlace.getPlace();
                //console.log(place.geometry.location.lat());
                scope.create.evData.latitude = place.geometry.location.lat();
                scope.create.evData.longitude = place.geometry.location.lng();
                scope.$apply(function() {
                    model.$setViewValue(element.val());                
                });
            });
        }
    };
})