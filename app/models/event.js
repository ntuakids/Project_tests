var mongoose = require('mongoose'); // Import Mongoose Package
var Schema = mongoose.Schema; // Assign Mongoose Schema function to variable
var validate = require('mongoose-validator'); // Import Mongoose Validator Plugin

// Title Validator
var titleValidator = [
    validate({
        validator: 'isLength',
        arguments: [3, 100],
        message: 'Title should be between {ARGS[0]} and {ARGS[1]} characters'
    })
];

// Description Validator
var descrValidator = [
    validate({
        validator: 'isLength',
        arguments: [3, 500],
        message: 'Description should be between {ARGS[0]} and {ARGS[1]} characters'
    })
];

/*// Event Mongoose Schema
var EventSchema = new Schema({
    provider: { type: String, required: true },
	title: { type: String, required: true },
    description: { type: String, required: true },	
	price: { type: String, required: true },
    address: { type: String, required: true },
    lat: { type: String },
    lng: { type: String },
	date: { type: String, required: true },
    time: { type: String, required: true },
	category: { type: String },
    total: { type: Number },
	available: { type: Number, min: 0 },
	age: { type: String },
	picture: { type: String },
	created: { type: Date, default: Date.now },
	active: { type: Boolean, required: true, default: true }
});*/

// Event Mongoose Schema
var EventSchema = new Schema({
    title: { type: String, required: true, unique: true, validate: titleValidator },
	//description: { type: String, required: true, validate: descrValidator },
	description: { type: String },
	price: { type: Number, required: true, min:1 },
	address: { type: String, required: true },
	total: { type: Number, required: true, min:1 },
	avail: { type: Number, required: true},
	date: { type: String, required: true },
    time: { type: String, required: true },
	category: { type: String, required: true },
	datefull: { type: Date, required: true },
	min_age: { type: Number, required: true },
	max_age: { type: Number, required: true },
	picture: { type: String, default: '/upload/event/test/1.png'},
	_provider: {type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },
	location: {type: [Number], required: true} // [Long, Lat]
	//provider: {type: String, required: true }
});

// Middleware to ensure everything is ok
EventSchema.pre('save', function(next) {

	// TODO
	return next();
	
});

// Indexes this schema in 2dsphere format (critical for running proximity searches)
EventSchema.index({location: '2dsphere'});

module.exports = mongoose.model('Event', EventSchema); // Export Event Model for us in API
