var mongoose = require('mongoose'); // Import Mongoose Package
var Schema = mongoose.Schema; // Assign Mongoose Schema function to variable

// User Mongoose Schema
var OrderSchema = new Schema({
    customer: { type: Schema.Types.ObjectId, required: true }, //alternative way is ->> type: string
    provider: { type: Schema.Types.ObjectId, required: true },	//alternative way is ->> type: string
	event: { type: Schema.Types.ObjectId, required: true }, //alternative way is ->> type: string
    created: { type: Date, default: Date.now },
	total: { type: Number },
	amount: { type: Number, min: 0 },
	pdf: { type: String }	
});

// Middleware to ensure everything is ok
OrderSchema.pre('save', function(next) {

	// TODO
	return next();
	
});

module.exports = mongoose.model('Order', OrderSchema); // Export Event Model for us in API
