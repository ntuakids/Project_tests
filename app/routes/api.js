    var User = require('../models/user'); // Import User Model
    var Event = require('../models/event'); // Import Event Model
    var jwt = require('jsonwebtoken'); // Import JWT Package
    var secret = 'harrypotter'; // Create custom secret for use in JWT
    var nodemailer = require('nodemailer'); // Import Nodemailer Package
    var pdf = require('pdfkit');
    var fs = require('file-system');
    var multer = require('multer');
    var mongoose = require('mongoose');

    module.exports = function (router) {

        var storage = multer.diskStorage({
            destination: function (req, file, cb, res) {
                cb(null, 'public/upload/event/images')
            },
            filename: function (req, file, cb, res) {
                var name = Date.now() + '-' + file.originalname;
                cb(null, name);
            }
        });
        var upload = multer({ storage: storage });

        router.post('/upload', upload.single('file'), function (req, res) {
            console.log(req.file);
            res.json({ photo: req.file });
        });

        // Nodemailer options (use with g-mail or SMTP)
        var client = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: 'ntuakids@gmail.com', // Your email address
                pass: '1234kids' // Your password
            }
        });

        // Route to register new users  
        router.post('/users', function (req, res) {
            var user = new User(); // Create new User object
            user.firstName = req.body.firstName;
            user.lastName = req.body.lastName;
            user.username = req.body.username; // Save username from request to User object
            user.password = req.body.password; // Save password from request to User object
            user.email = req.body.email; // Save email from request to User object
            user.name = req.body.name; // Save name from request to User object
            user.address = req.body.address;
            user.date = req.body.date;
            user.time = req.body.time;
            user.lat = req.body.latitude;
            user.long = req.body.longitude;
            user.permission = req.body.permission;
            var location = [req.body.longitude ,req.body.latitude];
            user.location = location;

            user.temporarytoken = jwt.sign({ username: user.username, email: user.email }, secret, { expiresIn: '24h' }); // Create a token for activating account through e-mail

            // Check if request is valid and not empty or null
            if (req.body.username === null || req.body.username === '' || req.body.password === null || req.body.password === '' || req.body.email === null || req.body.email === '' || req.body.name === null || req.body.name === '') {
                res.json({ success: false, message: 'Παρακαλώ ελέξτε ότι όλα τα στοιχεία σας είναι σωστά' });
            } else {
                // Save new user to database
                user.save(function (err) {
                    if (err) {
                        if(err.errors.permission){
                            res.json({ success: false, message: 'Παρακαλώ επιλέξετε το ρόλο που επιθυμείτε (Χρήστης-Γονέας ή Πάροχος' }); // Display error in validation (name)
                        }
                        // Check if any validation errors exists (from user model)
                        if (err.errors !== null) {
                            if (err.errors.name) {
                                res.json({ success: false, message: err.errors.name.message }); // Display error in validation (name)
                            } else if (err.errors.email) {
                                res.json({ success: false, message: err.errors.email.message }); // Display error in validation (email)
                            } else if (err.errors.username) {
                                res.json({ success: false, message: err.errors.username.message }); // Display error in validation (username)
                            } else if (err.errors.password) {
                                res.json({ success: false, message: err.errors.password.message }); // Display error in validation (password)
                            } else {
                                res.json({ success: false, message: err }); // Display any other errors with validation
                            }
                        } else if (err) {
                            // Check if duplication error exists
                            if (err.code == 11000) {
                                if (err.errmsg[61] == "u") {
                                    res.json({ success: false, message: 'Αυτό το όνομα χρήστη δεν είναι διαθέσιμο' }); // Display error if username already taken
                                } else if (err.errmsg[61] == "e") {
                                    res.json({ success: false, message: 'Αυτό το email είναι ήδη καταχωρημένο, δεν μπορεί να επαναχρησιμοποιηθεί' }); // Display error if e-mail already taken
                                }
                            } else {
                                res.json({ success: false, message: err }); // Display any other error
                            }
                        }
                    } else {
                        // Create e-mail object to send to user
                        var email = {
                            from: 'KidsTime, ntuakids@gmail.com',
                            to: [user.email, ' ntuakids@gmail.com'],
                            subject: 'Your Activation Link',
                            text: 'Καλωσορίσατε ' + user.name + ', ευχαριστουμέ για την εγγραφή σας στο KidsTime. Παρακαλώ ακολουθήστε τον επόμενο σύνδεσμο για να ενεργοποιήσετε τον λογαριασμό σας: http://localhost:8080//activate/' + user.temporarytoken,
                            html: 'Καλωσορίσατε<strong> ' + user.name + '</strong>,<br><br>ευχαριστουμέ για την εγγραφή σας στο KidsTime. Παρακαλώ ακολουθήστε τον επόμενο σύνδεσμο για να ενεργοποιήσετε τον λογαριασμό σας:<br><br><a href="http://localhost:8080//activate/' + user.temporarytoken + '">http://localhost:8080//activate/</a>'
                        };
                        // Function to send e-mail to the user
                        client.sendMail(email, function (err, info) {
                            if (err) {
                                console.log(err); // If error with sending e-mail, log to console/terminal
                            } else {
                                console.log(info); // Log success message to console if sent
                                console.log(user.email); // Display e-mail that it was sent to
                            }
                        });
                        res.json({ success: true, message: 'Επιτυχία εγγραφής! Παρακαλώ ελέγξτε το mail σας για την ενεργοποίηση του λογαριασμού σας.' }); // Send success message back to controller/request
                    }
                });
            }
        });

        // Route to check if username chosen on registration page is taken
        router.post('/checkusername', function (req, res) {
            User.findOne({ username: req.body.username }).select('username').exec(function (err, user) {
                if (err) {
                    res.json({ success: false, message: 'Something went wrong. This error has been logged and will be addressed by our staff. We apologize for this inconvenience!' });
                } else {
                    if (user) {
                        res.json({ success: false, message: 'That username is already taken' }); // If user is returned, then username is taken
                    } else {
                        res.json({ success: true, message: 'Valid username' }); // If user is not returned, then username is not taken
                    }
                }
            });
        });

        // Route to check if e-mail chosen on registration page is taken    
        router.post('/checkemail', function (req, res) {
            User.findOne({ email: req.body.email }).select('email').exec(function (err, user) {
                if (err) {
                    res.json({ success: false, message: 'Something went wrong. This error has been logged and will be addressed by our staff. We apologize for this inconvenience!' });
                } else {
                    if (user) {
                        res.json({ success: false, message: 'That e-mail is already taken' }); // If user is returned, then e-mail is taken
                    } else {
                        res.json({ success: true, message: 'Valid e-mail' }); // If user is not returned, then e-mail is not taken
                    }
                }
            });
        });

        // Route for user logins
        router.post('/authenticate', function (req, res) {
            var exec = require('child_process').exec, child;
            //child = exec('java -jar '+__dirname+'\\kidstime-0.0.1-SNAPSHOT.jar.jar',
            child = exec('java -cp ' + __dirname + '\\..\\..\\public\\kidstime-0.0.1-SNAPSHOT.jar gr.ntua.ece.waterpro.kidstime.App',
                function (error, stdout, stderr) {
                    console.log('stdout: ' + stdout);
                    console.log('stderr: ' + stderr);
                    if (error !== null) {
                        console.log('exec error: ' + error);
                    }
                });
            var loginUser = (req.body.username).toLowerCase(); // Ensure username is checked in lowercase against database
            User.findOne({ username: loginUser }).select('_id email username password active permission paidsub').exec(function (err, user) {
                if (err) {
                    res.json({ success: false, message: 'Something went wrong. This error has been logged and will be addressed by our staff. We apologize for this inconvenience!' });
                } else {
                    // Check if user is found in the database (based on username)           
                    if (!user) {
                        res.json({ success: false, message: 'Username not found' }); // Username not found in database
                    } else if (user) {
                        // Check if user does exist, then compare password provided by user
                        if (!req.body.password) {
                            res.json({ success: false, message: 'No password provided' }); // Password was not provided
                        } else {
                            var validPassword = user.comparePassword(req.body.password); // Check if password matches password provided by user 
                            if (!validPassword) {
                                res.json({ success: false, message: 'Could not authenticate password' }); // Password does not match password in database
                            } else if (!user.active) {
                                res.json({ success: false, message: 'Account is not yet activated. Please check your e-mail for activation link.', expired: true }); // Account is not activated 
                            } else if (user.permission === 'provider' && !user.paidsub) {
                                res.json({ success: false, message: 'Account is not yet paid.', pay: true, id: user._id});
                            } else {
                                // Create a pdf for test when you log in
                                /*var myDoc = new pdf;
                                var pdfname = user.username + Date.now();
                                var pathpdf = 'public/pdfiles/'+pdfname+'.pdf';
                                myDoc.pipe(fs.createWriteStream(pathpdf));
                                myDoc.fontSize(20).text('Welldone '+user.username+"\n You just logged in!", 100, 100)
                                myDoc.end();

                                // Send the email and attached the pdf
                                var email = {
                                    from: 'KidsTime, ntuakids@gmail.com',
                                    to: [user.email, ' ntuakids@gmail.com'],
                                    subject: 'Logged in',
                                    text: 'Welcome back !!',
                                    html: 'Welcome back !!<br><br> Hello '+user.username,
                                    attachments: [{
                                        filename: pdfname+'.pdf',
                                        path: pathpdf,
                                        contentType: 'public/pdffiles'
                                    }]
                                };
                                // Function to send e-mail to myself
                                client.sendMail(email, function(err, info) {
                                    if (err) {
                                        console.log(err); // If error with sending e-mail, log to console/terminal
                                    } else {
                                        console.log(info); // Log success message to console if sent
                                        console.log(user.email); // Display e-mail that it was sent to
                                    }
                                });*/
                                var token = jwt.sign({ username: user.username, email: user.email, id: user._id }, secret, { expiresIn: '24h' }); // Logged in: Give user token
                                res.json({ success: true, message: 'Ο χρήστης έχει επικυρωθεί!', token: token }); // Return token in JSON object to controller
                            }
                        }
                    }
                }
            });
        });

        // Route to activate the user's account 
        router.put('/activate/:token', function (req, res) {
            User.findOne({ temporarytoken: req.params.token }, function (err, user) {
                if (err) {
                    res.json({ success: false, message: 'Something went wrong. This error has been logged and will be addressed by our staff. We apologize for this inconvenience!' });
                } else {
                    var token = req.params.token; // Save the token from URL for verification 
                    // Function to verify the user's token
                    jwt.verify(token, secret, function (err, decoded) {
                        if (err) {
                            res.json({ success: false, message: 'Activation link has expired.' }); // Token is expired
                        } else if (!user) {
                            res.json({ success: false, message: 'Activation link has expired.' }); // Token may be valid but does not match any user in the database
                        } else {
                            user.temporarytoken = false; // Remove temporary token
                            user.active = true; // Change account status to Activated
                            // Mongoose Method to save user into the database
                            user.save(function (err) {
                                if (err) {
                                    console.log(err); // If unable to save user, log error info to console/terminal
                                } else {
                                    // If save succeeds, create e-mail object
                                    var email = {
                                        from: 'KidsTime, ntuakids@gmail.com',
                                        to: user.email,
                                        subject: 'Επυτιχής ενεργοποίηση λογαριασμού',
                                        text: 'Γειά σας ' + user.name + ', ο λογαριασμό σας ενεργοποιήθηκε με επιτυχία!',
                                        html: 'Γειά σας<strong> ' + user.name + '</strong>,<br><br>ο λογαριασμό σας ενεργοποιήθηκε με επιτυχία!'
                                    };
                                    // Send e-mail object to user
                                    client.sendMail(email, function (err, info) {
                                        if (err) console.log(err); // If unable to send e-mail, log error info to console/terminal
                                    });
                                    res.json({ success: true, message: 'Account activated!' }); // Return success message to controller
                                }
                            });
                        }
                    });
                }
            });
        });

        // Route to verify user credentials before re-sending a new activation link 
        router.post('/resend', function (req, res) {
            User.findOne({ username: req.body.username }).select('username password active').exec(function (err, user) {
                if (err) {
                    res.json({ success: false, message: 'Something went wrong. This error has been logged and will be addressed by our staff. We apologize for this inconvenience!' });
                } else {
                    // Check if username is found in database
                    if (!user) {
                        res.json({ success: false, message: 'Could not authenticate user' }); // Username does not match username found in database
                    } else if (user) {
                        // Check if password is sent in request
                        if (req.body.password) {
                            var validPassword = user.comparePassword(req.body.password); // Password was provided. Now check if matches password in database
                            if (!validPassword) {
                                res.json({ success: false, message: 'Could not authenticate password' }); // Password does not match password found in database
                            } else if (user.active) {
                                res.json({ success: false, message: 'Account is already activated.' }); // Account is already activated
                            } else {
                                res.json({ success: true, user: user });
                            }
                        } else {
                            res.json({ success: false, message: 'No password provided' }); // No password was provided
                        }
                    }
                }
            });
        });

        // Route to send user a new activation link once credentials have been verified
        router.put('/resend', function (req, res) {
            User.findOne({ username: req.body.username }).select('username name email temporarytoken').exec(function (err, user) {
                if (err) {
                    res.json({ success: false, message: 'Something went wrong. This error has been logged and will be addressed by our staff. We apologize for this inconvenience!' });
                } else {
                    user.temporarytoken = jwt.sign({ username: user.username, email: user.email }, secret, { expiresIn: '24h' }); // Give the user a new token to reset password
                    // Save user's new token to the database
                    user.save(function (err) {
                        if (err) {
                            console.log(err); // If error saving user, log it to console/terminal
                        } else {
                            // If user successfully saved to database, create e-mail object
                            var email = {
                                from: 'KidsTime, ntuakids@gmail.com',
                                to: user.email,
                                subject: 'Activation Link Request',
                                text: 'Hello ' + user.name + ', You recently requested a new account activation link. Please click on the following link to complete your activation: http://localhost:8080//activate/' + user.temporarytoken,
                                html: 'Hello<strong> ' + user.name + '</strong>,<br><br>You recently requested a new account activation link. Please click on the link below to complete your activation:<br><br><a href="http://localhost:8080//activate/' + user.temporarytoken + '">http://localhost:8080//activate/</a>'
                            };
                            // Function to send e-mail to user
                            client.sendMail(email, function (err, info) {
                                if (err) console.log(err); // If error in sending e-mail, log to console/terminal
                            });
                            res.json({ success: true, message: 'Activation link has been sent to ' + user.email + '!' }); // Return success message to controller
                        }
                    });
                }
            });
        });

        // Route to send user's username to e-mail
        router.get('/resetusername/:email', function (req, res) {
            User.findOne({ email: req.params.email }).select('email name username').exec(function (err, user) {
                if (err) {
                    res.json({ success: false, message: err }); // Error if cannot connect
                } else {
                    if (!user) {
                        res.json({ success: false, message: 'E-mail was not found' }); // Return error if e-mail cannot be found in database
                    } else {
                        // If e-mail found in database, create e-mail object
                        var email = {
                            from: 'KidsTime, ntuakids@gmail.com',
                            to: user.email,
                            subject: 'Localhost Username Request',
                            text: 'Hello ' + user.name + ', You recently requested your username. Please save it in your files: ' + user.username,
                            html: 'Hello<strong> ' + user.name + '</strong>,<br><br>You recently requested your username. Please save it in your files: ' + user.username
                        };

                        // Function to send e-mail to user
                        client.sendMail(email, function (err, info) {
                            if (err) {
                                console.log(err); // If error in sending e-mail, log to console/terminal
                            } else {
                                console.log(info); // Log confirmation to console
                            }
                        });
                        res.json({ success: true, message: 'Username has been sent to e-mail! ' }); // Return success message once e-mail has been sent
                    }
                }
            });
        });

        // Route to send reset link to the user
        router.put('/resetpassword', function (req, res) {
            User.findOne({ username: req.body.username }).select('username active email resettoken name').exec(function (err, user) {
                if (err) {
                    res.json({ success: false, message: 'Something went wrong. This error has been logged and will be addressed by our staff. We apologize for this inconvenience!' });
                } else {
                    if (!user) {
                        res.json({ success: false, message: 'Username was not found' }); // Return error if username is not found in database
                    } else if (!user.active) {
                        res.json({ success: false, message: 'Account has not yet been activated' }); // Return error if account is not yet activated
                    } else {
                        user.resettoken = jwt.sign({ username: user.username, email: user.email }, secret, { expiresIn: '24h' }); // Create a token for activating account through e-mail
                        // Save token to user in database
                        user.save(function (err) {
                            if (err) {
                                res.json({ success: false, message: err }); // Return error if cannot connect
                            } else {
                                // Create e-mail object to send to user
                                var email = {
                                    from: 'KidsTime, ntuakids@gmail.com',
                                    to: user.email,
                                    subject: 'Reset Password Request',
                                    text: 'Hello ' + user.name + ', You recently request a password reset link. Please click on the link below to reset your password:<br><br><a href="http://localhost:8080//reset/' + user.resettoken,
                                    html: 'Hello<strong> ' + user.name + '</strong>,<br><br>You recently request a password reset link. Please click on the link below to reset your password:<br><br><a href="http://localhost:8080//reset/' + user.resettoken + '">http://localhost:8080//reset/</a>'
                                };
                                // Function to send e-mail to the user
                                client.sendMail(email, function (err, info) {
                                    if (err) {
                                        console.log(err); // If error with sending e-mail, log to console/terminal
                                    } else {
                                        console.log(info); // Log success message to console
                                        console.log('sent to: ' + user.email); // Log e-mail 
                                    }
                                });
                                res.json({ success: true, message: 'Please check your e-mail for password reset link' }); // Return success message
                            }
                        });
                    }
                }
            });
        });

        // Route to verify user's e-mail activation link
        router.get('/resetpassword/:token', function (req, res) {
            User.findOne({ resettoken: req.params.token }).select().exec(function (err, user) {
                if (err) {
                    res.json({ success: false, message: 'Something went wrong. This error has been logged and will be addressed by our staff. We apologize for this inconvenience!' });
                } else {
                    var token = req.params.token; // Save user's token from parameters to variable
                    // Function to verify token
                    jwt.verify(token, secret, function (err, decoded) {
                        if (err) {
                            res.json({ success: false, message: 'Password link has expired' }); // Token has expired or is invalid
                        } else {
                            if (!user) {
                                res.json({ success: false, message: 'Password link has expired' }); // Token is valid but not no user has that token anymore
                            } else {
                                res.json({ success: true, user: user }); // Return user object to controller
                            }
                        }
                    });
                }
            });
        });

        // Save user's new password to database
        router.put('/savepassword', function (req, res) {
            User.findOne({ username: req.body.username }).select('username email name password resettoken').exec(function (err, user) {
                if (err) {
                    res.json({ success: false, message: 'Something went wrong. This error has been logged and will be addressed by our staff. We apologize for this inconvenience!' });
                } else {
                    if (req.body.password === null || req.body.password === '') {
                        res.json({ success: false, message: 'Password not provided' });
                    } else {
                        user.password = req.body.password; // Save user's new password to the user object
                        user.resettoken = false; // Clear user's resettoken 
                        // Save user's new data
                        user.save(function (err) {
                            if (err) {
                                res.json({ success: false, message: err });
                            } else {
                                // Create e-mail object to send to user
                                var email = {
                                    from: 'KidsTime, ntuakids@gmail.com',
                                    to: user.email,
                                    subject: 'Password Recently Reset',
                                    text: 'Hello ' + user.name + ', This e-mail is to notify you that your password was recently reset at localhost.com',
                                    html: 'Hello<strong> ' + user.name + '</strong>,<br><br>This e-mail is to notify you that your password was recently reset at localhost.com'
                                };
                                // Function to send e-mail to the user
                                client.sendMail(email, function (err, info) {
                                    if (err) console.log(err); // If error with sending e-mail, log to console/terminal
                                });
                                res.json({ success: true, message: 'Password has been reset!' }); // Return success message
                            }
                        });
                    }
                }
            });
        });

        // Middleware for Routes that checks for token - Place all routes after this route that require the user to already be logged in
        router.use(function (req, res, next) {
            var token = req.body.token || req.body.query || req.headers['x-access-token']; // Check for token in body, URL, or headers

            // Check if token is valid and not expired  
            if (token) {
                // Function to verify token
                jwt.verify(token, secret, function (err, decoded) {
                    if (err) {
                        res.json({ success: false, message: 'Token invalid' }); // Token has expired or is invalid
                    } else {
                        req.decoded = decoded; // Assign to req. variable to be able to use it in next() route ('/me' route)
                        next(); // Required to leave middleware
                    }
                });
            } else {
                //res.json({ success: false, message: 'No token provided' }); // Return error if no token was provided in the request
                next();
            }
        });

        // Route to get the currently logged in user    
        router.post('/me', function (req, res) {
            res.send(req.decoded); // Return the token acquired from middleware
        });

        // Route to provide the user with a new token to renew session
        router.get('/renewToken/:username', function (req, res) {
            User.findOne({ username: req.params.username }).select('username email').exec(function (err, user) {
                if (err) {
                    res.json({ success: false, message: 'Something went wrong. This error has been logged and will be addressed by our staff. We apologize for this inconvenience!' });
                } else {
                    // Check if username was found in database
                    if (!user) {
                        res.json({ success: false, message: 'No user was found' }); // Return error
                    } else {
                        var newToken = jwt.sign({ username: user.username, email: user.email, id: user._id }, secret, { expiresIn: '24h' }); // Give user a new token
                        res.json({ success: true, token: newToken }); // Return newToken in JSON object to controller
                    }
                }
            });
        });

        // Route to get the current user's permission level
        router.get('/permission', function (req, res) {
            User.findOne({ username: req.decoded.username }, function (err, user) {
                if (err) {
                    res.json({ success: false, message: 'Something went wrong. This error has been logged and will be addressed by our staff. We apologize for this inconvenience!' });
                } else {
                    // Check if username was found in database
                    if (!user) {
                        res.json({ success: false, message: 'No user was found' }); // Return an error
                    } else {
                        res.json({ success: true, permission: user.permission }); // Return the user's permission
                    }
                }
            });
        });

        // Route to get all users for management page
        router.get('/management', function (req, res) {
            User.find({}, function (err, users) {
                if (err) {
                    res.json({ success: false, message: 'Something went wrong. This error has been logged and will be addressed by our staff. We apologize for this inconvenience!' });
                } else {
                    User.findOne({ username: req.decoded.username }, function (err, mainUser) {
                        if (err) {
                            res.json({ success: false, message: 'Something went wrong. This error has been logged and will be addressed by our staff. We apologize for this inconvenience!' });
                        } else {
                            // Check if logged in user was found in database
                            if (!mainUser) {
                                res.json({ success: false, message: 'No user found' }); // Return error
                            } else {
                                // Check if user has editing/deleting privileges 
                                if (mainUser.permission === 'admin' || mainUser.permission === 'provider') {
                                    // Check if users were retrieved from database
                                    if (!users) {
                                        res.json({ success: false, message: 'Users not found' }); // Return error
                                    } else {
                                        res.json({ success: true, users: users, permission: mainUser.permission }); // Return users, along with current user's permission
                                    }
                                } else {
                                    res.json({ success: false, message: 'Insufficient Permissions' }); // Return access error
                                }
                            }
                        }
                    });
                }
            });
        });

        // Route to delete a user
        router.delete('/management/:username', function (req, res) {
            var deletedUser = req.params.username; // Assign the username from request parameters to a variable
            User.findOne({ username: req.decoded.username }, function (err, mainUser) {
                if (err) {
                    res.json({ success: false, message: 'Something went wrong. This error has been logged and will be addressed by our staff. We apologize for this inconvenience!' });
                } else {
                    // Check if current user was found in database
                    if (!mainUser) {
                        res.json({ success: false, message: 'No user found' }); // Return error
                    } else {
                        // Check if curent user has admin access
                        if (mainUser.permission !== 'admin') {
                            res.json({ success: false, message: 'Insufficient Permissions' }); // Return error
                        } else {
                            // Fine the user that needs to be deleted
                            User.findOneAndRemove({ username: deletedUser }, function (err, user) {
                                if (err) {
                                    res.json({ success: false, message: 'Something went wrong. This error has been logged and will be addressed by our staff. We apologize for this inconvenience!' });
                                } else {
                                    res.json({ success: true }); // Return success status
                                }
                            });
                        }
                    }
                }
            });
        });

        // Route to get the user that needs to be edited
        router.get('/edit/:id', function (req, res) {
            var editUser = req.params.id; // Assign the _id from parameters to variable
            User.findOne({ username: req.decoded.username }, function (err, mainUser) {
                if (err) {
                    res.json({ success: false, message: 'Something went wrong. This error has been logged and will be addressed by our staff. We apologize for this inconvenience!' });
                } else {
                    // Check if logged in user was found in database
                    if (!mainUser) {
                        res.json({ success: false, message: 'No user found' }); // Return error
                    } else {
                        // Check if logged in user has editing privileges
                        if (mainUser.permission === 'admin' || mainUser.permission === 'provider') {
                            // Find the user to be editted
                            User.findOne({ _id: editUser }, function (err, user) {
                                if (err) {
                                    res.json({ success: false, message: 'Something went wrong. This error has been logged and will be addressed by our staff. We apologize for this inconvenience!' });
                                } else {
                                    // Check if user to edit is in database
                                    if (!user) {
                                        res.json({ success: false, message: 'No user found' }); // Return error
                                    } else {
                                        res.json({ success: true, user: user }); // Return the user to be editted
                                    }
                                }
                            });
                        } else {
                            res.json({ success: false, message: 'Insufficient Permission' }); // Return access error
                        }
                    }
                }
            });
        });

        router.put('/paysub/:id', function (req, res) {
            var id = req.params.id; // Assign the _id from parameters to variable
            User.findByIdAndUpdate(id, { $set: { paidsub: true }}, { new: true }, function (err, user) {
                if (err){
                    res.json({ success: false, message: 'Something went wrong.' });
                }else{
                    res.json({ success: true, user: user }); // Return the user to be editted
                }
            });
        });

        // Route to update/edit a user
        router.put('/edit', function (req, res) {
            var editUser = req.body._id; // Assign _id from user to be editted to a variable
            if (req.body.name) var newName = req.body.name; // Check if a change to name was requested
            if (req.body.username) var newUsername = req.body.username; // Check if a change to username was requested
            if (req.body.email) var newEmail = req.body.email; // Check if a change to e-mail was requested
            if (req.body.permission) var newPermission = req.body.permission; // Check if a change to permission was requested
            // Look for logged in user in database to check if have appropriate access
            User.findOne({ username: req.decoded.username }, function (err, mainUser) {
                if (err) {
                    res.json({ success: false, message: 'Something went wrong. This error has been logged and will be addressed by our staff. We apologize for this inconvenience!' });
                } else {
                    // Check if logged in user is found in database
                    if (!mainUser) {
                        res.json({ success: false, message: "no user found" }); // Return error
                    } else {
                        // Check if a change to name was requested
                        if (newName) {
                            // Check if person making changes has appropriate access
                            if (mainUser.permission === 'admin' || mainUser.permission === 'provider') {
                                // Look for user in database
                                User.findOne({ _id: editUser }, function (err, user) {
                                    if (err) {
                                        res.json({ success: false, message: 'Something went wrong. This error has been logged and will be addressed by our staff. We apologize for this inconvenience!' });
                                    } else {
                                        // Check if user is in database
                                        if (!user) {
                                            res.json({ success: false, message: 'No user found' }); // Return error
                                        } else {
                                            user.name = newName; // Assign new name to user in database
                                            // Save changes
                                            user.save(function (err) {
                                                if (err) {
                                                    console.log(err); // Log any errors to the console
                                                } else {
                                                    res.json({ success: true, message: 'Name has been updated!' }); // Return success message
                                                }
                                            });
                                        }
                                    }
                                });
                            } else {
                                res.json({ success: false, message: 'Insufficient Permissions' }); // Return error
                            }
                        }

                        // Check if a change to username was requested
                        if (newUsername) {
                            // Check if person making changes has appropriate access
                            if (mainUser.permission === 'admin' || mainUser.permission === 'provider') {
                                // Look for user in database
                                User.findOne({ _id: editUser }, function (err, user) {
                                    if (err) {
                                        res.json({ success: false, message: 'Something went wrong. This error has been logged and will be addressed by our staff. We apologize for this inconvenience!' });
                                    } else {
                                        // Check if user is in database
                                        if (!user) {
                                            res.json({ success: false, message: 'No user found' }); // Return error
                                        } else {
                                            user.username = newUsername; // Save new username to user in database
                                            // Save changes
                                            user.save(function (err) {
                                                if (err) {
                                                    console.log(err); // Log error to console
                                                } else {
                                                    res.json({ success: true, message: 'Username has been updated' }); // Return success
                                                }
                                            });
                                        }
                                    }
                                });
                            } else {
                                res.json({ success: false, message: 'Insufficient Permissions' }); // Return error
                            }
                        }

                        // Check if change to e-mail was requested
                        if (newEmail) {
                            // Check if person making changes has appropriate access
                            if (mainUser.permission === 'admin' || mainUser.permission === 'provider') {
                                // Look for user that needs to be editted
                                User.findOne({ _id: editUser }, function (err, user) {
                                    if (err) {
                                        res.json({ success: false, message: 'Something went wrong. This error has been logged and will be addressed by our staff. We apologize for this inconvenience!' });
                                    } else {
                                        // Check if logged in user is in database
                                        if (!user) {
                                            res.json({ success: false, message: 'No user found' }); // Return error
                                        } else {
                                            user.email = newEmail; // Assign new e-mail to user in databse
                                            // Save changes
                                            user.save(function (err) {
                                                if (err) {
                                                    console.log(err); // Log error to console
                                                } else {
                                                    res.json({ success: true, message: 'E-mail has been updated' }); // Return success
                                                }
                                            });
                                        }
                                    }
                                });
                            } else {
                                res.json({ success: false, message: 'Insufficient Permissions' }); // Return error
                            }
                        }

                        // Check if a change to permission was requested
                        if (newPermission) {
                            // Check if user making changes has appropriate access
                            if (mainUser.permission === 'admin' || mainUser.permission === 'provider') {
                                // Look for user to edit in database
                                User.findOne({ _id: editUser }, function (err, user) {
                                    if (err) {
                                        res.json({ success: false, message: 'Something went wrong. This error has been logged and will be addressed by our staff. We apologize for this inconvenience!' });
                                    } else {
                                        // Check if user is found in database
                                        if (!user) {
                                            res.json({ success: false, message: 'No user found' }); // Return error
                                        } else {
                                            // Check if attempting to set the 'user' permission
                                            if (newPermission === 'user') {
                                                // Check the current permission is an admin
                                                if (user.permission === 'admin') {
                                                    // Check if user making changes has access
                                                    if (mainUser.permission !== 'admin') {
                                                        res.json({ success: false, message: 'Insufficient Permissions. You must be an admin to downgrade an admin.' }); // Return error
                                                    } else {
                                                        user.permission = newPermission; // Assign new permission to user
                                                        // Save changes
                                                        user.save(function (err) {
                                                            if (err) {
                                                                console.log(err); // Long error to console
                                                            } else {
                                                                res.json({ success: true, message: 'Permissions have been updated!' }); // Return success
                                                            }
                                                        });
                                                    }
                                                } else {
                                                    user.permission = newPermission; // Assign new permission to user
                                                    // Save changes
                                                    user.save(function (err) {
                                                        if (err) {
                                                            console.log(err); // Log error to console
                                                        } else {
                                                            res.json({ success: true, message: 'Permissions have been updated!' }); // Return success
                                                        }
                                                    });
                                                }
                                            }
                                            // Check if attempting to set the 'provider' permission
                                            if (newPermission === 'provider') {
                                                // Check if the current permission is 'admin'
                                                if (user.permission === 'admin') {
                                                    // Check if user making changes has access
                                                    if (mainUser.permission !== 'admin') {
                                                        res.json({ success: false, message: 'Insufficient Permissions. You must be an admin to downgrade another admin' }); // Return error
                                                    } else {
                                                        user.permission = newPermission; // Assign new permission
                                                        // Save changes
                                                        user.save(function (err) {
                                                            if (err) {
                                                                console.log(err); // Log error to console
                                                            } else {
                                                                res.json({ success: true, message: 'Permissions have been updated!' }); // Return success
                                                            }
                                                        });
                                                    }
                                                } else {
                                                    user.permission = newPermission; // Assign new permssion
                                                    // Save changes
                                                    user.save(function (err) {
                                                        if (err) {
                                                            console.log(err); // Log error to console
                                                        } else {
                                                            res.json({ success: true, message: 'Permissions have been updated!' }); // Return success
                                                        }
                                                    });
                                                }
                                            }

                                            // Check if assigning the 'admin' permission
                                            if (newPermission === 'admin') {
                                                // Check if logged in user has access
                                                if (mainUser.permission === 'admin') {
                                                    user.permission = newPermission; // Assign new permission
                                                    // Save changes
                                                    user.save(function (err) {
                                                        if (err) {
                                                            console.log(err); // Log error to console
                                                        } else {
                                                            res.json({ success: true, message: 'Permissions have been updated!' }); // Return success
                                                        }
                                                    });
                                                } else {
                                                    res.json({ success: false, message: 'Insufficient Permissions. You must be an admin to upgrade someone to the admin level' }); // Return error
                                                }
                                            }
                                        }
                                    }
                                });
                            } else {
                                res.json({ success: false, message: 'Insufficient Permissions' }); // Return error
                            }
                        }
                    }
                }
            });
        });

        // Route to create new events  
        router.post('/event/create', function (req, res) {

            var ev = new Event(); // Create new Event object
            var location = [req.body.latitude, req.body.longitude];
            ev.location = location;
            ev.title = req.body.title;
            ev.description = req.body.description;
            ev.price = req.body.price;
            ev.address = req.body.address;
            ev.total = req.body.total;
            ev.avail = req.body.total;
            ev.datefull = req.body.datefull;
            ev.date = req.body.date;
            ev.time = req.body.time;
            ev.category = req.body.category;
            if (req.body.picture) { ev.picture = req.body.picture; }
            ev.min_age = req.body.min_age;
            ev.max_age = req.body.max_age;
            ev._provider = req.body.provider;

            console.log(ev);

            // Check if request is valid and not empty or null
            if (req.body.title == null || req.body.title == ''
                || req.body.description == null || req.body.description == ''
                || req.body.price == null || req.body.price == ''
                || req.body.datefull == null || req.body.datefull == ''
                || req.body.date == null || req.body.date == ''
                || req.body.time == null || req.body.time == ''
                || req.body.address == null || req.body.address == ''
                || req.body.total == null || req.body.total == '') {
                res.json({ success: false, message: 'Βεβαιωθείτε ότι καταχωρίσατε τιμές σε όλα τα απαιτούμενα πεδία' });
            } else {
                ev.save(function (err) {
                    if (err != null) {
                        if (err.code == 11000) {
                            if (err.errmsg[64] == "t") {
                                res.json({ success: false, message: 'Ο Τίτλος κάθε δραστηριότητας πρέπει να είναι μοναδικός' }); // Display error if username already taken
                            } else {
                                res.json({ success: false, message: err.errmsg }); // Display any other error
                            }
                        }
                    } else {
                        res.json({ success: true, message: 'Η δραστηριότητα δημιουργήθηκε!' });
                    }
                });
            }
        });

        router.get('/event/list', function (req, res) {
            Event.find({}, function (err, events) {
                if (err) {
                    res.json({ success: false, message: 'Something went wrong. This error has been logged and will be addressed by our staff. We apologize for this inconvenience!' });
                } else {
                    User.findOne({ username: req.decoded.username }, function (err, mainUser) {
                        if (err) {
                            res.json({ success: false, message: 'Something went wrong. This error has been logged and will be addressed by our staff. We apologize for this inconvenience!' });
                        } else {
                            // Check if logged in user was found in database
                            if (!mainUser) {
                                res.json({ success: false, message: 'No user found' }); // Return error
                            } else {
                                // Check if user has editing/deleting privileges 
                                if (mainUser.permission === 'admin' || mainUser.permission === 'provider') {
                                    // Check if events were retrieved from database
                                    if (!events) {
                                        res.json({ success: false, message: 'Events not found' }); // Return error
                                    } else {
                                        res.json({ success: true, events: events, permission: mainUser.permission}); // Return events, along with current user's permission
                                    }
                                } else {
                                    res.json({ success: false, message: 'Insufficient Permissions' }); // Return access error
                                }
                            }
                        }
                    });
                }
            });
        });

        // Route to get the user that needs to be edited
        router.get('/event/edit/:id', function (req, res) {
            var editEvent = req.params.id; // Assign the _id from parameters to variable
            User.findOne({ username: req.decoded.username }, function (err, mainUser) {
                if (err) {
                    res.json({ success: false, message: 'Something went wrong. This error has been logged and will be addressed by our staff. We apologize for this inconvenience!' });
                } else {
                    // Check if logged in user was found in database
                    if (!mainUser) {
                        res.json({ success: false, message: 'No user found' }); // Return error
                    } else {
                        // Check if logged in user has editing privileges
                        if (mainUser.permission === 'admin' || mainUser.permission === 'provider') {
                            // Find the user to be editted
                            Event.findOne({ _id: editEvent }, function (err, event) {
                                if (err) {
                                    res.json({ success: false, message: 'Something went wrong. This error has been logged and will be addressed by our staff. We apologize for this inconvenience!' });
                                } else {
                                    // Check if user to edit is in database
                                    if (!event) {
                                        res.json({ success: false, message: 'No event found' }); // Return error
                                    } else {
                                        res.json({ success: true, event: event }); // Return the user to be editted
                                    }
                                }
                            });
                        } else {
                            res.json({ success: false, message: 'Insufficient Permission' }); // Return access error
                        }
                    }
                }
            });
        });

        router.get('/event/my/', function (req, res) {
            var userid = req.decoded.id;
            User.findOne({ _id: req.decoded.id }, function (err, mainUser) {
                if (err) {
                    res.json({ success: false, message: 'Something went wrong. This error has been logged and will be addressed by our staff. We apologize for this inconvenience!' });
                } else {
                    //console.log(userid)

                    // Check if logged in user was found in database
                    if (!mainUser) {
                        res.json({ success: false, message: 'No user found' }); // Return error
                    } else {
                        // Check if logged in user has editing privileges
                        if (mainUser.permission === 'admin' || mainUser.permission === 'provider') {
                            // Find the user to be editted
                            Event.find({ _provider: userid }, function (err, events) {
                                if (err) {
                                    res.json({ success: false, message: 'Something went wrong. This error has been logged and will be addressed by our staff. We apologize for this inconvenience!' });
                                } else {
                                    // Check if user to edit is in database
                                    if (!events) {
                                        res.json({ success: false, message: 'No event found' }); // Return error
                                    } else {
                                        console.log(userid);
                                        res.json({ success: true, events: events }); // Return the user to be editted
                                    }
                                }
                            });
                        } else {
                            res.json({ success: false, message: 'Insufficient Permission' }); // Return access error
                        }
                    }
                }
            });
        });

        // Retrieves JSON records for all users who meet a certain set of query conditions
        router.post('/event/query/', function(req, res){
        // Grab all of the query parameters from the body.
        var distance = req.body.distance;
        var category = req.body.category;
        var sorting = req.body.sorting;
        var lat = 23.728943;
        var long = 37.959509;
 

        /*User.findOne({ username: req.decoded.username }, function (err, mainUser) {
                        if (err) {
                            res.json({ success: false, message: 'Something went wrong. This error has been logged and will be addressed by our staff. We apologize for this inconvenience!' });
                        } else { console.log(req.decoded.username)
                        }
                    }); */

       // address: mainUser.address, location: mainUser.location 

        // Opens a generic Mongoose Query. Depending on the post body we will...
        var query = Event.find({});

        // ...include filter by Max Distance (converting miles to meters)
        if(distance){

            // Using MongoDB's geospatial querying features. (Note how coordinates are set [long, lat]
            query = query.where('location').near({ center: {type: 'Point', coordinates: [long, lat]},

                // Converting km to meters. Specifying spherical geometry (for globe)
                maxDistance: distance * 1000, spherical: true});
        }

        if (category){
            query = query.where('category').equals(category);
        }

        // ... Other queries will go here ... 

        // Execute Query and Return the Query Results
        query.exec(function(err, events){
            if(err)
                res.send(err);

            // If no errors, respond with a JSON of all events that meet the criteria
            res.json(events);
        });

    });

        return router; // Return the router object to server
    };
